#!/usr/bin/env python3

import sys;
input_file=open(sys.argv[1], "r");

def get_next_line():
    while 1:
        line=input_file.readline();
        if(len(line) == 0):
            return None
        if len(line) > 1:
            line_split=line.split();
            return line_split;

completed_tasks={};
started_tasks={};

def update_stats(worker):
    task_name=started_tasks[worker]["task"];
    start_date=float(started_tasks[worker]["start_date"]);
    stop_date=float(started_tasks[worker]["stop_date"]);
    duration = stop_date-start_date;
        
    stat=completed_tasks.get(task_name);
    if(stat == None):
        completed_tasks[task_name]={"name":task_name,
                                    "ntasks":0,
                                    "total_duration":0,
                                    "min":-1,
                                    "max":-1};
        stat = completed_tasks[task_name];
    stat["ntasks"] += 1;
    stat["total_duration"] += duration;
    if(stat["min"]<0 or stat["min"] > duration):
        stat["min"] = duration;

    if(stat["max"]<0 or stat["max"] < duration):
        stat["max"] = duration;

def print_stats():
    print("#task\tntasks\ttotal_duration\tmin_duration\tmax_duration\tavg_duration");
    for key, value in completed_tasks.items():
        task=value;
        avg=task["total_duration"]/task["ntasks"];
        print(task["name"]+"\t"+str(task["ntasks"])+"\t"+str(task["total_duration"])+"\t"+
              str(task["min"])+"\t"+str(task["max"])+"\t"+str(avg));

        
while 1:
    line_split=get_next_line();
    if line_split==None :
        break
    if (len(line_split)>0):
        if line_split[0]=="20":
            timestamp=line_split[1];
            worker=line_split[2];
            task=line_split[4];

            if(started_tasks.get(worker)!=None and float(started_tasks[worker]["start_date"]) > 0):
                sys.stderr.write("Error at "+timestamp+": worker "+worker+" did not finish its previous job (started at"+started_tasks[worker]["start_date"]+")\n");
            
            started_tasks[worker]={}
            started_tasks[worker]["start_date"]=timestamp;
            started_tasks[worker]["task"]=task;

                #        print(timestamp+": worker "+worker+" starts task "+task);
        if line_split[0]=="10" and line_split[4] == "\"Po\"":
            timestamp=line_split[1];
            worker=line_split[2];

            if(started_tasks.get(worker)!=None and float(started_tasks[worker]["start_date"]) < 0):
                sys.stderr.write("Error at "+timestamp+": worker "+worker+" finished a job that never started!\n");

            duration=float(timestamp)-float(started_tasks[worker]["start_date"]);
                #        print(timestamp+": worker "+worker+" ends his task("+started_tasks[worker]["task"]+") that started at "+started_tasks[worker]["start_date"]+" -> duration: "+str(duration));

            started_tasks[worker]["stop_date"]=timestamp;
            update_stats(worker);

            started_tasks[worker]["start_date"]=-1;



print_stats()

